import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { SystemScan, Host } from './types';

const group_api = '/api/v1/group';

@Injectable({
    providedIn: 'root'
})
export class GroupService {

    constructor(
       private _http: HttpClient,
    ) {}

    update(by: string[]): Observable<SystemScan[]> {
        let params = new HttpParams();
        params = params.set('update', by.join(','));
        return this._http.put<SystemScan[]>(group_api, {}, {
            responseType: 'json',
            params: params
        });
    }


    fetch(target: '*'|string[]): Observable<Host[]> {
        let params = new HttpParams();
        if (target === '*') {
            params = params.set('search', '*');
        } else {
            params = params.set('search', target.join(','));
        }
        return this._http.get<SystemScan[]>(group_api, {params: params});
    }



    delete(byName: string): Observable<void> {
        return new Observable((observer) => {
            if (byName) {
                const params = new HttpParams()
                    .set('target', byName);
                this._http.delete(group_api, {
                    params: params,
                }).subscribe((data) => {
                    observer.next();
                }, (err) => {
                    observer.error(err);
                });
                return;
            }
            observer.error('Name was not specified');
        });
    }
}
