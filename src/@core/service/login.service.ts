import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { User, LoginStatus } from './types';
import {CookieService} from 'ngx-cookie-service';

const user_api = '/api/v1/user';
const login_api = '/api/v1/isLogin';
const getuser_api = '/api/v1/getuser';

@Injectable({
    providedIn: 'root'
})
export class LoginService {


    constructor(
        private _http: HttpClient,
        private cookieService: CookieService,
    ) {}

    create(ob: User): Observable<any> {
        let userParam = new HttpParams();
        userParam = userParam.set('user', JSON.stringify(ob));
        return this._http.post(user_api, {}, {
            responseType: 'json',
            params: userParam
        });
    }

    update(ob: User): Observable<LoginStatus> {
        let userParam = new HttpParams();
        userParam = userParam.set('user', JSON.stringify(ob));
        return this._http.put(user_api, {}, {
            responseType: 'json',
            params: userParam
        });
    }
    delete(byName): Observable<any> {
        return new Observable((observer) => {
            if (byName) {
                const params = new HttpParams()
                    .set('target', byName);
                this._http.delete(user_api, {
                    params: params,
                }).subscribe((data) => {
                    observer.next();
                }, (err) => {
                    observer.error(err);
                });
                return;
            }
            observer.error('Name was not specified');
        });
    }

    login(ob: User): Observable<LoginStatus> {
        let param = new HttpParams();
        param = param.set('userType', ob.userType);
        return this._http.get(user_api, {
            headers: {
                'Authorization': 'Basic ' + btoa(ob.name + ':' + ob.password)
            },
            params: param
        });
    }

    get isLoggedIn (): any {
        return this.cookieService.getAll();
    }

    getUser(target: '*'|string[]) {
        let params = new HttpParams();
        if (target === '*') {
            params = params.set('search', '*');
        } else {
            params = params.set('search', target.join(','));
        }
        return this._http.get<User[]>(getuser_api, {params: params});
    }
}
