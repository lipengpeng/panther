
import { NgModule } from '@angular/core';
import { DelayDirective, TitleDirective, EditorDirective, ChartDirective } from '@core/directive';
import { HostService, ExecService, GroupService, LoginService, CveService, HistoryService } from '@core/service';
import { FindPipe } from './pipe';

@NgModule({
    declarations: [
        DelayDirective,
        EditorDirective,
        TitleDirective,
        FindPipe,
        ChartDirective
    ],
    exports: [
        DelayDirective,
        EditorDirective,
        TitleDirective,
        FindPipe,
        ChartDirective,
    ],
    providers: [
        HostService,
        ExecService,
        GroupService,
        LoginService,
        CveService,
        HistoryService
    ]
})
export class CoreModule {}
