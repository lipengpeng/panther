import { Component, OnInit, OnDestroy } from '@angular/core';
import { deepCopy } from '@core/utils';
import { SystemScan, ExecWebsocket, ExecService, LoginService } from '@core';
import { Subscription } from 'rxjs';
import { HeaderService } from 'app/header';
// import { EChartsOption, EChartsOptionConfig } from 'echarts';

@Component({
    selector: 'app-overview',
    templateUrl: './overview.component.html',
    styleUrls: ['./overview.component.scss']
})

export class AppOverviewComponent implements OnInit, OnDestroy {

    private scan: ExecWebsocket;
    private scanSubscription: Subscription;

    optionData: any;

    option: any = {
        title : {
            text: 'CVE 补丁统计',
            // subtext: '纯属虚构',
            x: 'center'
        },
        tooltip : {
            trigger: 'item',
            formatter: '{a} <br/>{b} : {c} ({d}%)'
        },
        legend: {
            type: 'scroll',
            orient: 'vertical',
            right: 10,
            top: 20,
            bottom: 20,

        },
        series : [
            {
                name: 'CVE',
                type: 'pie',
                radius : '45%',
                center: ['50%', '50%'],
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            },
        ],
        color: ['#F44336',  '#2196F3', '#9C27B0'],
    };

    barOption = {
        xAxis: {
            type: 'category',
            data: ['有危险补丁主机', '无危险补丁主机'],
        },
        yAxis: {
            type: 'value',
            name: '主机数',
        },
        series: [{
            // data: [120, 200, 150, 80, 70, 110, 130],
            type: 'bar',
            itemStyle: {
                normal: {
                    color: function(params) {
                        // build a color map as your need.
                        const colorList = [
                          '#F4511E', '#009688',
                        ];
                        return colorList[params.dataIndex];
                    },
                }
            },
            barWidth: 70,
        }],
    };
    // deviceOption: EChartsOption;
    deviceOption: any;
    // criticalOption: EChartsOption;
    criticalOption: any;
    constructor(
        private exec: ExecService,
        private _header: HeaderService,
        private cookie: LoginService,
    ) {}
    ngOnInit() {
        this._header.title = '概览图';
        this.scan = this.exec.forScan('*');
        this.scanSubscription =  this.scan.connect().subscribe((data) => {
            const cookie = this.cookie.isLoggedIn;
            let tableData = [];
            if ( cookie && JSON.stringify(cookie) !== '{}') {
                for (const key in cookie) {
                    if (key !== 'admin') {
                        for (const val of data) {
                            if (val.metadata.user === key) {
                                tableData.push(val);
                            }
                        }
                    } else {
                        tableData = deepCopy(data);
                    }
                }
            }
            this.optionData =  this.getData(tableData);
            this.deviceOption = this.option;
            this.deviceOption.legend.data = this.optionData.legendData;
            this.deviceOption.series[0].data = this.optionData.seriesData;
            this.deviceOption.title.subtext = '主机数量共' + this.optionData.hostNum + '台';
            this.criticalOption = this.barOption;
            this.criticalOption.series[0].data = this.filterCritical(tableData);
        });
    }
    ngOnDestroy() {}

    getData(data: SystemScan[]) {
        const SystemScanList = deepCopy(data);
        const legendData = [];
        const seriesData = [];
        let criticalNum = 0;
        let importantNum = 0;
        let ModerateNum = 0;
        const hostNum = SystemScanList.length;
        for (const val of SystemScanList) {
            if (val.state === 5) {
                continue;
            }
            const security = val.security;
            for (const value of security) {
                if (value.severity === 1) {
                    criticalNum++;
                }
                if (value.severity === 2) {
                    importantNum++;
                }
                if (value.severity === 3) {
                    ModerateNum++;
                }
            }
        }
        seriesData.push({
            name: '危险补丁',
            value: criticalNum
        });
        seriesData.push({
            name: '重要补丁',
            value: importantNum
        });
        seriesData.push({
            name: '中等补丁',
            value: ModerateNum
        });
        legendData.push('Critical Packages');
        legendData.push('Important Packages');
        legendData.push('Moderate Packages');
        return {
            legendData: legendData,
            seriesData: seriesData,
            hostNum: hostNum
        };
    }

    filterCritical (data: SystemScan[]) {
        const barData = [];
        let hasCritical = 0;
        let notHasCritical = 0;
        const SystemScanList = deepCopy(data);
        for (const val of SystemScanList) {
            if (val.state === 5) {
                continue;
            }
            const security = val.security;
            let criticalNum = 0;
            for (const value of security) {
                if (value.severity === 1) {
                    criticalNum++;
                }
            }
            if (criticalNum > 0) {
                hasCritical++;
            } else {
                notHasCritical++;
            }
        }
        barData.push(hasCritical, notHasCritical);
        return barData;
    }

}







