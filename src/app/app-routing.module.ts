import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppHostComponent } from './host';
import { AppExecScanComponent, AppExecScanDetailsComponent, AppCveComponent } from './exec';
import { AppGroupListComponent } from './grouplist';
import { AppGroupComponent } from './group';
import { AppAuthGuard } from './auth/auth.guard';
import { AppUserListComponent } from './userlist/userlist.component';
import { AppOverviewComponent } from './overview';
import { AppHistoryComponent } from './history';

const routes: Routes = [{
    path: 'overview',
    component: AppOverviewComponent,
    pathMatch: 'full',
    canActivate: [AppAuthGuard]
}, {
    path: 'scan',
    component: AppExecScanComponent,
    pathMatch: 'full',
    canActivate: [AppAuthGuard]
}, {
    path: 'scan/:name',
    component: AppExecScanDetailsComponent,
    pathMatch: 'full',
    canActivate: [AppAuthGuard]
}, {
    path: 'cve/:name',
    component: AppCveComponent,
    pathMatch: 'full',
    canActivate: [AppAuthGuard]
}, {
    path: 'host',
    component: AppHostComponent,
    pathMatch: 'full',
    canActivate: [AppAuthGuard]
},  {
    path: 'grouplist',
    component: AppGroupListComponent,
    pathMatch: 'full',
    canActivate: [AppAuthGuard]
}, {
    path: 'grouplist/:group',
    component: AppGroupComponent,
    pathMatch: 'full',
    canActivate: [AppAuthGuard]
}, {
    path: 'userlist',
    component: AppUserListComponent,
    pathMatch: 'full',
    canActivate: [AppAuthGuard]
}, {
    path: 'history',
    component: AppHistoryComponent,
    pathMatch: 'full',
    canActivate: [AppAuthGuard]
}, {
    path: '**',
    redirectTo: 'overview',
    pathMatch: 'full',
}];

@NgModule({
    imports: [RouterModule.forRoot(routes, {})],
    exports: [RouterModule]
})
export class AppRoutingModule {}
