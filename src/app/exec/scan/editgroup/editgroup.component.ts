import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SystemScan, GroupService } from '@core';

@Component({
    selector: 'app-edit-group',
    templateUrl: './editgroup.component.html',
    styleUrls: ['./editgroup.component.scss']
})

export class AppEditGroupComponent implements OnInit, OnDestroy {
    group: string;
    constructor(
        public dialogRef: MatDialogRef<AppEditGroupComponent>,
        @Inject(MAT_DIALOG_DATA) public data: SystemScan[],
        private _group: GroupService
    ) {}
    ngOnInit() {}
    ngOnDestroy() {}

    update() {
        const array = [];
        for (const each of this.data) {
            array.push(each.metadata.name);
        }
        array.push(this.group);
        this._group.update(array).subscribe(() => {
            this.dialogRef.close();
        });

    }

    close () {
        this.dialogRef.close();
    }
}
