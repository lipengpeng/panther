all: ready build

deps:
	@dep ensure -v
	@yarn

install: deps
	@go install gitlab.com/ivyent/panther/cmd/...

test: deps
	@go test -cpu 1,4 -timeout 5m gitlab.com/ivyent/panther/...
	@yarn run test

race: deps
	@go test -race -cpu 1,4 -timeout 7m gitlab.com/ivyent/panther/...

benchmark: deps
	@go test -bench . -cpu 1,4 -timeout 10m gitlab.com/ivyent/panther/...

build: deps
	go build gitlab.com/ivyent/panther/cmd/...
	yarn run build

devel: deps
	@yarn run start

clean:
	@go clean -i gitlab.com/ivyent/panther/...

ready:
	go version
	go get -u github.com/golang/dep/cmd/dep
	node -v
	@npm -g install @angular/cli
	@npm -g install yarn

.PHONY: \
	deps \
	test \
	build \
	devel \
	ready \
	dist \