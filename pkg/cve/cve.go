package cve

import (
	"encoding/csv"
	"os"
	"strings"
)

//CVEEntity 用来存储CVE信息
type CVEEntity struct {
	Name        string   `json:"name,omitempty"`        // CVE-ID
	Status      string   `json:"status,omitempty"`      //CVE的status
	Description string   `json:"description,omitempty"` //CVE的description
	References  []string `json:"references,omitempty"`  //CVE 的references
	Phase       string   `json:"phase,omitempty"`       //CVE 的phase
	Votes       []string `json:"votes,omitempty"`       //CVE 的votes
	Comments    []string `json:"comments,omitempty"`    //CVE 的comments
}

type SimpleCache struct {
	records map[string]*CVEEntity // map: Name => *CVEEntity
}

//Get 方法根据k值返回相应的CVE
func (c *SimpleCache) Get(k string) (*CVEEntity, error) {
	return c.records[k], nil
}

//NewSimpleCache 方法读取csv文件并将每个cve信息存到cache中
func NewSimpleCache(filename string) (cache *SimpleCache, err error) {
	var file *os.File
	if file, err = os.Open(filename); err != nil {
		return
	}
	r := csv.NewReader(file)
	r.FieldsPerRecord = -1
	var records [][]string
	if records, err = r.ReadAll(); err != nil {
		return
	}
	entities := make(map[string]*CVEEntity)
	for _, vals := range records {
		if len(vals) != 7 || !strings.HasPrefix(vals[0], "CVE-") {
			continue
		}
		name := strings.TrimSpace(vals[0])
		entities[name] = &CVEEntity{
			Name:        name,
			Status:      strings.TrimSpace(vals[1]),
			Description: strings.TrimSpace(vals[2]),
			References:  splitStrVector(vals[3], "|"),
			Phase:       strings.TrimSpace(vals[4]),
			Votes:       splitStrVector(vals[5], "|"),
			Comments:    splitStrVector(vals[6], "|"),
		}
	}
	return &SimpleCache{records: entities}, nil
}

//splitStrVector根据delim将字符串分隔
func splitStrVector(str, delim string) (ss []string) {
	strs := strings.Split(str, delim)
	for _, each := range strs {
		s := strings.TrimSpace(each)
		if s == "" {
			continue
		}
		ss = append(ss, s)
	}
	return
}
