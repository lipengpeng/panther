package web

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	genericStorage "gitlab.com/ivyent/panther/pkg/storage/generic"
)

func (in *Handler) History(w http.ResponseWriter, r *http.Request) {
	defer in.logger.Sync()
	defer func() {
		if rec := recover(); rec != nil {
			in.finalizeError(w, fmt.Errorf("Internal Server Error"), http.StatusInternalServerError)
			in.logger.Error(rec)
		}
	}()
	defer in.finalizeHeader(w)
	switch r.Method {
	case "GET":
		search := r.URL.Query().Get("search")
		cv := genericStorage.NewPackageHisty()
		cv.SetName(search)
		err := in.storage.Get(cv)
		if err != nil {
			in.finalizeError(w, fmt.Errorf("Database Failure"), http.StatusInternalServerError)
			in.logger.Error(err)
			return
		}
		dAtA, err := json.Marshal(cv)
		if err != nil {
			panic(err)
		}
		in.finalizeJSON(w, bytes.NewReader(dAtA))
	default:
		return
	}
}
