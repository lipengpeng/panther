package web

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
)

// Cve is used to get cve information and pass to front end
func (in *Handler) Cve(w http.ResponseWriter, r *http.Request) {
	defer in.logger.Sync()
	defer func() {
		if rec := recover(); rec != nil {
			in.finalizeError(w, fmt.Errorf("Internal Server Error"), http.StatusInternalServerError)
			in.logger.Error(rec)
		}
	}()
	defer in.finalizeHeader(w)
	switch r.Method {
	case "GET":
		targets := strings.Split(r.URL.Query().Get("search"), ",")
		for _, v := range targets {
			b, e := cveInfo.Get(strings.TrimSpace(v))
			if e != nil {
				fmt.Println("err = ", e)
			}
			data, err := json.Marshal(b)
			if err != nil {
				panic(err)
			}
			in.finalizeJSON(w, bytes.NewReader(data))
		}
	default:
		return
	}

}
