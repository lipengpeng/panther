package web

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"

	genericStorage "gitlab.com/ivyent/panther/pkg/storage/generic"
)

//Group is handler of upadating group information or get SystemScanList due to group information
func (in *Handler) Group(w http.ResponseWriter, r *http.Request) {
	defer in.logger.Sync()
	defer func() {
		if rec := recover(); rec != nil {
			in.finalizeError(w, fmt.Errorf("Internal Server Error"), http.StatusInternalServerError)
			in.logger.Error(rec)
		}
	}()
	defer in.finalizeHeader(w)
	switch r.Method {
	case "GET":
		targets := strings.Split(r.URL.Query().Get("search"), ",")
		if len(targets) == 1 && targets[0] == "" {
			in.finalizeError(w, fmt.Errorf("Target required"), http.StatusBadRequest)
			in.logger.Errorf("No target was specified during querey")
			return
		}
		in.logger.Debugf("Search group: %s", strings.Join(targets, ","))
		var all bool
		for _, each := range targets {
			if each == "*" {
				all = true
				break
			}
		}
		sortor := genericStorage.NewSortor()
		if all {
			cv := genericStorage.NewSystemScanList()
			err := in.storage.List(cv)
			if err != nil {
				in.finalizeError(w, fmt.Errorf("Database Failure"), http.StatusInternalServerError)
				return
			}
			for i := range cv.Members {
				sortor.AppendMember(&cv.Members[i])
			}
		} else {
			for _, each := range targets {
				newObj := genericStorage.NewSystemScan()
				newObj.Group = each
				err := in.storage.Get(newObj)
				if err != nil {
					in.logger.Error(err)
					if genericStorage.IsInternalError(err) {
						in.finalizeStorageError(w, err)
						return
					}
					in.finalizeError(w, fmt.Errorf("Database Failure"), http.StatusInternalServerError)
					return
				}
				sortor.AppendMember(newObj)
			}
		}
		dAtA, err := json.Marshal(sortor.OrderByName())
		if err != nil {
			panic(err)
		}
		in.finalizeJSON(w, bytes.NewReader(dAtA))

	case "PUT":
		frontedData := strings.Split(r.URL.Query().Get("update"), ",")
		length := len(frontedData) - 1
		fmt.Fprintf(os.Stderr, "update host is ==%v\n", strings.Join(frontedData[:length], ","))
		group := frontedData[length]
		hostlist := frontedData[:length]
		for _, each := range hostlist {
			if each == group {
				return
			}
			host := genericStorage.NewSystemScan()
			host.SetName(each)
			err := in.storage.Get(host)
			if err != nil {
				in.finalizeError(w, fmt.Errorf("Can not get data of "+each+"from database"), http.StatusInternalServerError)
			}
			host.SetGroup(group)
			err = in.storage.Update(host)
			if err != nil {
				in.logger.Error(err)
				if genericStorage.IsInternalError(err) {
					in.finalizeError(w, fmt.Errorf("Database Failure"), http.StatusInternalServerError)
					return
				}
			}

		}
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}

}
