package web

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"path"
	"strconv"
	"strings"

	genericStorage "gitlab.com/ivyent/panther/pkg/storage/generic"
)

// BatchCreate is using Batch create host by readding csv file
func (in *Handler) BatchCreate(w http.ResponseWriter, r *http.Request) {
	defer in.logger.Sync()
	defer func() {
		if rec := recover(); rec != nil {
			in.finalizeError(w, fmt.Errorf("Internal Server Error"), http.StatusInternalServerError)
			in.logger.Error(rec)
		}
	}()
	defer in.finalizeHeader(w)
	switch r.Method {
	case "POST":
		r.ParseMultipartForm(32 << 20)
		file, fileHeader, err := r.FormFile("uploadfile")
		if err != nil {
			in.logger.Error(err)
		}
		defer file.Close()
		ext := path.Ext(fileHeader.Filename)
		if ext != ".csv" {
			in.finalizeError(w, fmt.Errorf("File format is incorrect"), http.StatusForbidden)
			return
		}
		f, err := os.Create(fileHeader.Filename)
		if err != nil {
			in.logger.Error(err)
			return
		}
		defer f.Close()
		_, err = io.Copy(f, file)
		if err != nil {
			in.logger.Error(err)
		}
		hostfile, err := os.Open(fileHeader.Filename)
		if err != nil {
			in.logger.Error(err)
			return
		}
		defer hostfile.Close()
		rd := bufio.NewReader(hostfile)
		for {
			line, err := rd.ReadString('\n')
			if err != nil {
				if err == io.EOF {
					break
				}
			}
			hostArr := strings.Split(string(line), ",")
			if strings.Replace(hostArr[0], " ", "", -1) != "SequenceNumber" {
				if hostArr[1] != "" && hostArr[2] != "" && hostArr[3] != "" && hostArr[4] != "" && hostArr[5] != "" {
					cv := genericStorage.NewHost()
					var host genericStorage.Host
					var buf bytes.Buffer

					host.ObjectMeta.Name = strings.Replace(hostArr[1], " ", "", -1)
					host.SSHAddress = strings.Replace(hostArr[2], " ", "", -1)
					port, err := strconv.ParseInt(hostArr[3], 10, 16)
					if err != nil {
						in.logger.Error(err)
						continue
					}
					host.SSHPort = uint16(port)
					host.SSHCredential.User = strings.Replace(hostArr[4], " ", "", -1)
					host.SSHCredential.Password = []byte(strings.Replace(hostArr[5], " ", "", -1))
					host.OpCredential.User = strings.Replace(hostArr[6], " ", "", -1)
					host.OpCredential.Password = []byte(strings.Replace(hostArr[7], " ", "", -1))
					host.Comment = strings.Replace(hostArr[8], " ", "", -1)
					user := strings.Replace(hostArr[9], " ", "", -1)
					ur := strings.Replace(user, "\r", "", -1)
					host.User = strings.Replace(ur, "\n", "", -1)

					data, _ := json.Marshal(host)
					_, err = io.Copy(&buf, bytes.NewReader([]byte(data)))
					if err != nil {
						in.logger.Error(err)
						continue
					}
					json.Unmarshal(buf.Bytes(), cv)
					err = in.validateAndFulfillHost(cv)
					if err != nil {
						in.logger.Error(err)
						in.finalizeError(w, err, http.StatusBadRequest)
						continue
					}
					err = in.storage.Create(cv)
					if err != nil {
						in.logger.Error(err)
						if genericStorage.IsInternalError(err) {
							in.finalizeStorageError(w, err)
							continue
						}
						in.finalizeError(w, fmt.Errorf("Database Failure"), http.StatusInternalServerError)
						continue
					}
				}
			}
		}
	}
}
