package web

import (
	"bytes"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"time"

	genericStorage "gitlab.com/ivyent/panther/pkg/storage/generic"
	"golang.org/x/crypto/bcrypt"
)

// User handles requests from /api/v1/user.
// Usage:
//   - GET /api/v1/user?search=[]
//   - POST /api/v1/user
//   - PUT /api/v1/user
//   - DELETE /api/v1/user?target=[User_NAME]
// TODO: make logger content qualified.
// User function uses to generate 、update 、delete a user information
var sessionMgr *SessionMgr
var sessionID string

//User handler is using to handle log in update user create user and delete user
func (in *Handler) User(w http.ResponseWriter, r *http.Request) {
	defer in.logger.Sync()
	defer func() {
		if rec := recover(); rec != nil {
			in.finalizeError(w, fmt.Errorf("Internal Server Error"), http.StatusInternalServerError)
			in.logger.Error(rec)
		}
	}()
	defer in.finalizeHeader(w)

	switch r.Method {
	case "GET":
		username, password, userType := GetBasic(w, r, in)
		cv := genericStorage.NewUser()
		cv.SetName(username)
		err := in.storage.Get(cv)
		if err != nil {
			in.logger.Errorf("can not get %s information due to %v", username, err)
			return
		}
		err = bcrypt.CompareHashAndPassword(cv.Password, []byte(password))
		if err != nil {
			in.logger.Error("This password is incorrect err is ", err)
			return
		}
		userType = strings.TrimSpace(userType)
		if userType != strings.TrimSpace(cv.Usertype) {
			in.logger.Error("This user type is incorrect")
			return
		}
		sessionMgr = NewSessionMgr(username, 86400)
		sessionID = sessionMgr.StartSession(w, r)
		var loginUserInfo = genericStorage.User{Name: username, Password: []byte(password), Usertype: userType}
		sessionMgr.SetSessionVal(sessionID, "UserInFo", loginUserInfo)

		var state genericStorage.LoginStatus
		state.Details = "login successful"
		state.Status = 1
		data, err := json.Marshal(state)
		if err != nil {
			panic(err)
		}
		in.finalizeJSON(w, bytes.NewReader(data))

	case "POST":
		var user User
		err := json.Unmarshal([]byte(r.URL.Query().Get("user")), &user)
		if err != nil {
			in.logger.Error("The data sent from the parsing front end is incorrect. The error is", err)
		}
		username := user.Name
		password := user.Password
		userType := user.UserType

		pwd, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
		if err != nil {
			return
		}
		cv := genericStorage.NewUser()
		cv.Name = username
		cv.Password = pwd
		cv.Usertype = userType
		cv.SetName(username)
		err = in.storage.Create(cv)
		if err != nil {
			in.logger.Error(err)
			if genericStorage.IsInternalError(err) {
				in.finalizeStorageError(w, err)
				return
			}
			in.finalizeError(w, fmt.Errorf("Database Failure"), http.StatusInternalServerError)
			return
		}
		var state genericStorage.LoginStatus
		state.Status = 1
		state.Details = "Create successful"
		data, err := json.Marshal(state)
		if err != nil {
			panic(err)
		}
		in.finalizeJSON(w, bytes.NewReader(data))
	case "PUT":
		var user User
		err := json.Unmarshal([]byte(r.URL.Query().Get("user")), &user)
		if err != nil {
			in.logger.Error("The data sent from the parsing front end is incorrect. The error is", err)
		}
		username := user.Name
		password := user.Password
		userType := user.UserType
		pwd, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
		if err != nil {
			return
		}
		cv := genericStorage.NewUser()
		cv.Name = username
		cv.Password = pwd
		cv.Usertype = userType
		cv.SetName(username)
		err = in.storage.Update(cv)
		if err != nil {
			in.logger.Errorf("Updating %s is failed,due to %v", cv.Name, err)
		}
		var state genericStorage.LoginStatus
		state.Status = 1
		state.Details = "update successful"
		data, err := json.Marshal(state)
		if err != nil {
			panic(err)
		}
		in.finalizeJSON(w, bytes.NewReader(data))
	case "DELETE":
		target := r.URL.Query().Get("target")
		if target == "" {
			in.finalizeError(w, fmt.Errorf("Target required"), http.StatusBadRequest)
			in.logger.Errorf("No user target was specified")
			return
		}
		cv := genericStorage.NewUser()
		cv.SetName(target)
		err := in.storage.Delete(cv)
		if err != nil {
			in.logger.Errorf("Delete %s is failed,due to %v", cv.GetName(), err)
			return
		}
		var state genericStorage.LoginStatus
		state.Status = 1
		state.Details = "delete successful"
		data, err := json.Marshal(state)
		if err != nil {
			panic(err)
		}
		in.finalizeJSON(w, bytes.NewReader(data))
	}

}

type User struct {
	Name     string
	Password string
	UserType string
}

//GetBasic uses to get user infomation
func GetBasic(w http.ResponseWriter, r *http.Request, in *Handler) (username string, password string, userType string) {
	userType = r.URL.Query().Get("userType")
	auth := r.Header.Get("Authorization")
	if auth == "" {
		w.Header().Set("WWW-Authenticate", `Basic realm="Dotcoo User Login"`)
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	auths := strings.SplitN(auth, " ", 2)
	if len(auths) != 2 {
		in.logger.Error("missing username or password")
		return
	}
	authMethod := auths[0]
	authB64 := auths[1]
	switch authMethod {
	case "Basic":
		authstr, err := base64.StdEncoding.DecodeString(authB64)
		if err != nil {
			fmt.Println(err)
			io.WriteString(w, "Unauthorized!\n")
			return
		}
		userPwd := strings.SplitN(string(authstr), ":", 2)
		if len(userPwd) != 2 {
			fmt.Println("error")
			return
		}
		username = strings.Replace(userPwd[0], " ", "", -1)
		password = strings.Replace(userPwd[1], " ", "", -1)
		return username, password, userType
	default:
		fmt.Println("error")
		return
	}
}

type SessionMgr struct {
	mCookieName  string
	mLock        sync.RWMutex
	mMaxLifeTime int64
	mSessions    map[string]*Session
}

//创建会话管理器(cookieName:在浏览器中cookie的名字，maxLifeTime:最长生命周期)
func NewSessionMgr(cookieName string, maxLifetime int64) *SessionMgr {
	mgr := &SessionMgr{mCookieName: cookieName, mMaxLifeTime: maxLifetime, mSessions: make(map[string]*Session)}
	go mgr.GC()
	return mgr

}

//在开始页面登陆页面，开始Session
func (mgr *SessionMgr) StartSession(w http.ResponseWriter, r *http.Request) string {
	mgr.mLock.Lock()
	defer mgr.mLock.Unlock()

	//无论原来有没有，都重新创建一个新的session
	newSessionID := url.QueryEscape(mgr.NewSessionID())

	//存指针
	var session = &Session{mSessionID: newSessionID, mLastTimeAccessed: time.Now(), mValues: make(map[interface{}]interface{})}
	mgr.mSessions[newSessionID] = session
	//让浏览器cookie设置过期时间
	cookie := http.Cookie{Name: mgr.mCookieName, Value: newSessionID, Path: "/", MaxAge: int(mgr.mMaxLifeTime)}
	http.SetCookie(w, &cookie)

	return newSessionID
}

//结束Session
func (mgr *SessionMgr) EndSession(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie(mgr.mCookieName)
	if err != nil || cookie.Value == "" {
		return
	} else {
		mgr.mLock.Lock()
		defer mgr.mLock.Unlock()

		delete(mgr.mSessions, cookie.Value)

		//让浏览器cookie立刻过期
		expiration := time.Now()
		cookie := http.Cookie{Name: mgr.mCookieName, Path: "/", Expires: expiration, MaxAge: -1}
		http.SetCookie(w, &cookie)
	}
}

//结束session
func (mgr *SessionMgr) EndSessionBy(sessionID string) {
	mgr.mLock.Lock()
	defer mgr.mLock.Unlock()

	delete(mgr.mSessions, sessionID)
}

//设置session里面的值
func (mgr *SessionMgr) SetSessionVal(sessionID string, key interface{}, value interface{}) {
	mgr.mLock.Lock()
	defer mgr.mLock.Unlock()

	if session, ok := mgr.mSessions[sessionID]; ok {
		session.mValues[key] = value
	}
}

//得到session里面的值
func (mgr *SessionMgr) GetSessionVal(sessionID string, key interface{}) (interface{}, bool) {
	mgr.mLock.RLock()
	defer mgr.mLock.RUnlock()

	if session, ok := mgr.mSessions[sessionID]; ok {
		if val, ok := session.mValues[key]; ok {
			return val, ok
		}
	}

	return nil, false
}

//得到sessionID列表
func (mgr *SessionMgr) GetSessionIDList() []string {
	mgr.mLock.RLock()
	defer mgr.mLock.RUnlock()

	sessionIDList := make([]string, 0)

	for k, _ := range mgr.mSessions {
		sessionIDList = append(sessionIDList, k)
	}

	return sessionIDList[0:len(sessionIDList)]
}

//判断Cookie的合法性（每进入一个页面都需要判断合法性）
func (mgr *SessionMgr) CheckCookieValid(w http.ResponseWriter, r *http.Request) string {
	var cookie, err = r.Cookie(mgr.mCookieName)

	if cookie == nil ||
		err != nil {
		return ""
	}

	mgr.mLock.Lock()
	defer mgr.mLock.Unlock()

	sessionID := cookie.Value

	if session, ok := mgr.mSessions[sessionID]; ok {
		session.mLastTimeAccessed = time.Now() //判断合法性的同时，更新最后的访问时间
		return sessionID
	}

	return ""
}

//更新最后访问时间
func (mgr *SessionMgr) GetLastAccessTime(sessionID string) time.Time {
	mgr.mLock.RLock()
	defer mgr.mLock.RUnlock()

	if session, ok := mgr.mSessions[sessionID]; ok {
		return session.mLastTimeAccessed
	}

	return time.Now()
}

// 创建唯一的ID
func (mgr *SessionMgr) NewSessionID() string {
	b := make([]byte, 32)
	if _, err := io.ReadFull(rand.Reader, b); err != nil {
		nano := time.Now().UnixNano() //微秒
		return strconv.FormatInt(nano, 10)
	}
	return base64.URLEncoding.EncodeToString(b)
}

//GC回收
func (mgr *SessionMgr) GC() {
	mgr.mLock.Lock()
	defer mgr.mLock.Unlock()

	for sessionID, session := range mgr.mSessions {
		//删除超过时限的session
		if session.mLastTimeAccessed.Unix()+mgr.mMaxLifeTime < time.Now().Unix() {
			delete(mgr.mSessions, sessionID)
		}
	}

	//定时回收
	time.AfterFunc(time.Duration(mgr.mMaxLifeTime)*time.Second, func() { mgr.GC() })
}

type Session struct {
	mSessionID        string
	mLastTimeAccessed time.Time //最后访问时间
	mValues           map[interface{}]interface{}
}
